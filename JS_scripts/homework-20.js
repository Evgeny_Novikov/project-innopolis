"use strict";

//Упражнение-1

for(let x = 0;x < 21;x++){
  if(x % 2 !== 0) continue;
  console.log(x);
}

//Упражнение-2

let sum = 0;
for (let c = 0;c < 3;c++) {
  let favoritNumber1 = Number(prompt("Введите любимое число используя только цифры: "));
  if (!favoritNumber1){
    alert("Ошибка, вы ввели не число");
    break;
  }
  sum += favoritNumber1;
  console.log(sum);
}

//Упражнение-3

let m = Number(prompt("Enter your favorite month:"));

function getNameOfMonth(m) {
  if(m === 0) return "январь";
  if(m === 1) return "февраль";
  if(m === 2) return "март";
  if(m === 3) return "апрель";
  if(m === 4) return "май";
  if(m === 5) return "июнь";
  if(m === 6) return "июль";
  if(m === 7) return "август";
  if(m === 8) return "сентябрь";
  if(m === 9) return "октябрь";
  if(m === 10) return "ноябрь";
  if(m === 11) return "декабрь";
}

for(let z = 0; z < 12; z++) {
  const y = getNameOfMonth(z);
  if(z === 9) continue;
  console.log(y);
};

let p = getNameOfMonth(m);
alert(p);