"use strict";
//упражнение-1

let a = '100px';
let b = '323px';

let result = parseInt(a) + parseInt(b);
console.log(result);

//Упражнение-2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Упражнение-3

let d = 0.111;
console.log(Math.ceil(d))

//Упражнение-4

let e = 45.333333;
console.log(e.toFixed(1))

//Упражнение-5

let f = 3;
console.log(f ** 5)

//Упражнение-6

let g = 4e14;
console.log(g);

//Упражнение-7

let h = '1';
console.log(h == 1);

//Дополнительное Упражнение-Просто стандартная функция вывода не показывает всех знаков после точки.

console.log("{0:R}", 0.1 + 0.2);


