"use strict";

let form = document.querySelector(".form__wrap");

let inputNameContainer = form.querySelector(".form__input--name-container");
let inputScoreContainer = form.querySelector(".form__input--score-container");

let inputName = inputNameContainer.querySelector(".form__input--font-text");
let errorNameElem = inputNameContainer.querySelector(".form__error_name");

let inputScore = inputScoreContainer.querySelector(".form__input--font-number");
let errorScoreElem = inputScoreContainer.querySelector(".form__error_score");

window.onload=function(){
let inputNameFocus = inputName.addEventListener("focus", handleFocusName);
let inputScoreFocus = inputScore.addEventListener("focus", handleFocusScore);
};


function handleSubmit(event,) {
	event.preventDefault();

//For name
  let errorName = "";

	let name = inputName.value;

  if (name === "") {

  errorName = 'Вы забыли указать имя и фамилию';
  errorNameElem.innerText = errorName;
  errorNameElem.classList.toggle("form__error--name", true);


  } else if (name.length < 2) {

    errorName = 'Имя не может быть короче 2-х символов';
    errorNameElem.innerText = errorName;
    errorNameElem.classList.toggle("form__error--name", true);

  } else {

    errorNameElem.classList.toggle("form__error--name", false);
    errorNameElem.innerText = errorName;
  };

  
  
  //For score

  let errorScore = "";

  let score = inputScore.value;

  if (score === "") {

    errorScore = "Вы забыли указать оценку от 1 до 5";
    errorScoreElem.innerText = errorScore;
    errorScoreElem.classList.toggle("form__error--score", true);

  }else if (score < 1 || score > 5) {

    errorScore = "Оценка должна быть от 1 до 5";
    errorScoreElem.innerText = errorScore;
    errorScoreElem.classList.toggle("form__error--score", true);

  } else {

    errorScoreElem.classList.toggle("form__error--score", false);
    errorScoreElem.innerText = errorScore;

  };
  
};

function handleFocusName(inputNameFocus) {
  let errorFocusName = "";

  if (inputNameFocus) {
    
    errorNameElem.classList.toggle("form__error--name", false);
    errorNameElem.innerText = errorFocusName;

  };
    
};

function handleFocusScore(inputScoreFocus) {
    let errorFocusName = "";

  if (inputScoreFocus) {
    
    errorScoreElem.classList.toggle("form__error--score", false);
    errorScoreElem.innerText = errorFocusName;
  };
};


form.addEventListener('submit', handleSubmit);