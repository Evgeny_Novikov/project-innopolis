"use strict";

/*

let intervalId = setInterval(function() {
  
  number--;

  console.log(number);

  if (number === 0) {
    clearInterval(intervalId);
    console.log("отсчёт завершён!");
  };
  
}, 2000);*/
let number = Number(prompt("Введите число с которог нужно начать отсчёт:"));

let promise = new Promise( function(resolve, reject) {
  if (Number(number)) {
    
    resolve();
  };
  if (!Number(number)) {
    reject();
  };
  });


promise
.then( function(){
  //console.log("Начало отсчёта");
  let intervalId = setInterval(function() {
    number--;
    console.log(number);
    if (number === 0) {
      clearInterval(intervalId);
      console.log("Ваше время вышло!");
    };
  
  }, 1000);
})
.catch( function(){
  console.log("Вы ввели не число!")
});
console.time("time");
let promises = fetch("https://reqres.in/api/users");
console.timeEnd("time");

promises
.then(function(response) {
  return response.json();
})
.then(function(response) {
  console.log(`Получили пользователей:${response["per_page"]}`);
  let users = response["data"];
  users.forEach(function(element){
  console.log(`— ${element["first_name"]} ${element["last_name"]} (${element["email"]})`);
  });
})
.catch(function() {
  console.log("Кажется бэкенд сломался :(");
});


