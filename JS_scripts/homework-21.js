"use strict"

//Упражнение-1

let user = {};
//user.name = "Evgeny";

/**
 * Возвращает true,если у объекта нет свойств, иначе false.
 * @param {Object} Проветка наличия свойств в объекте.
 * @return {Boolean} Проверенный обьект на наличие свойств.
 */
function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}
let u = isEmpty(user);
alert(u);

//Упражнение-3

let salaries= {
  John:100000,
  Ann:160000,
  Pete:130000,
};

function raiseSalary(percent) {
  let newSalaries = {};
  for ( let key in salaries) {
    let percentSalaries = ((salaries[key] * percent) / 100) + salaries[key];
    newSalaries[key] = percentSalaries;
  };
  return newSalaries;
};

let obj = raiseSalary(5);
console.log(obj);


let calcSumm = 0;
/**
 * Суммирует значения увеличенные на (n %).
 * @param {object}  Принимает обьект для последущей обработки значений.
 * @returns Возращает итоговую сумму всех значений.
 */

function summ(obj) {
  for (let key in obj) {
    calcSumm += obj[key];
  };
  return calcSumm;
};

let totalSamm = summ(obj);
console.log(totalSamm);